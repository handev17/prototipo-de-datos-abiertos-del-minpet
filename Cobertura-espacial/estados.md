| Subdivision Estado     | Identificador Prototipo | Identificador ISO | Identificador UBIGEO-IGVSB-INE |
|------------------------|-------------------------|-------------------|--------------------------------|
| Distrito Capital       | edo-dtto-capital-ve     | VE-A              | 1                              |
| Anzoátegui             | edo-anzoategui-ve       | VE-B              | 3                              |
| Apure                  | edo-apure-ve            | VE-C              | 4                              |
| Aragua                 | edo-aragua-ve           | VE-D              | 5                              |
| Barinas                | edo-barinas-ve          | VE-E              | 6                              |
| Bolívar                | edo-bolivar-ve          | VE-F              | 7                              |
| Carabobo               | edo-carabobo-ve         | VE-G              | 8                              |
| Cojedes                | edo-cojedes-ve          | VE-H              | 9                              |
| Falcón                 | edo-falcon-ve           | VE-I              | 11                             |
| Guárico                | edo-guarico-ve          | VE-J              | 12                             |
| Lara                   | edo-lara-ve             | VE-K              | 13                             |
| Mérida                 | edo-merida-ve           | VE-L              | 14                             |
| Miranda                | edo-miranda-ve          | VE-M              | 15                             |
| Monagas                | edo-monagas-ve          | VE-N              | 16                             |
| Nueva Esparta          | edo-nva-esparta-ve      | VE-O              | 17                             |
| Portuguesa             | edo-portuguesa-ve       | VE-P              | 18                             |
| Sucre                  | edo-sucre-ve            | VE-R              | 19                             |
| Táchira                | edo-tachira-ve          | VE-S              | 20                             |
| Trujillo               | edo-trujillo-ve         | VE-T              | 21                             |
| Yaracuy                | edo-yaracuy-ve          | VE-U              | 22                             |
| Zulia                  | edo-zulia-ve            | VE-V              | 23                             |
| Dependencias Federales | depen-federales-ve      | VE-W              | 25                             |
| Vargas                 | edo-vargas-ve           | VE-X              | 24                             |
| Delta Amacuro          | edo-delta-amacuro-ve    | VE-Y              | 10                             |
| Amazonas               | edo-amazonas-ve         | VE-Z              | 2                              |