| Estado                 | Municipio                                   | Identificador prototipo                   | Identificador UBIGEO-IGVSB |
|------------------------|---------------------------------------------|-------------------------------------------|----------------------------|
| Distrito Capital       | Municipio Bolivariano Libertador            | mpio-libertador-edo-dtto-capital-ve       | 10100                      |
| Anzoátegui             | Municipio Anaco                             | mpio-anaco-edo-anzoategui-ve              | 30100                      |
| Anzoátegui             | Municipio Aragua                            | mpio-aragua-edo-anzoategui-ve             | 30200                      |
| Anzoátegui             | Municipio Fernando de Peñalver              | mpio-penalver-edo-anzoategui-ve           | 30300                      |
| Anzoátegui             | Municipio Francisco del Carmen Carvajal     | mpio-carvajal-edo-anzoategui-ve           | 30400                      |
| Anzoátegui             | Municipio Francisco de Miranda              | mpio-miranda-edo-anzoategui-ve            | 30500                      |
| Anzoátegui             | Municipio Guanta                            | mpio-guanta-edo-anzoategui-ve             | 30600                      |
| Anzoátegui             | Municipio Independencia                     | mpio-independencia-edo-anzoategui-ve      | 30700                      |
| Anzoátegui             | Municipio Juan Antonio Sotillo              | mpio-sotillo-edo-anzoategui-ve            | 30800                      |
| Anzoátegui             | Municipio Juan Manuel Cajigal               | mpio-cajigal-edo-anzoategui-ve            | 30900                      |
| Anzoátegui             | Municipio José Gregorio Monagas             | mpio-monagas-edo-anzoategui-ve            | 31000                      |
| Anzoátegui             | Municipio Libertad                          | mpio-libertad-edo-anzoategui-ve           | 31100                      |
| Anzoátegui             | Municipio Manuel Ezequiel Bruzual           | mpio-bruzual-edo-anzoategui-ve            | 31200                      |
| Anzoátegui             | Municipio Pedro María Fréites               | mpio-freites-edo-anzoategui-ve            | 31300                      |
| Anzoátegui             | Municipio Píritu                            | mpio-piritu-edo-anzoategui-ve             | 31400                      |
| Anzoátegui             | Municipio San José de Guanipa               | mpio-guanipa-edo-anzoategui-ve            | 31500                      |
| Anzoátegui             | Municipio San Juan de Capistrano            | mpio-capistrano-edo-anzoategui-ve         | 31600                      |
| Anzoátegui             | Municipio Santa Ana                         | mpio-santa-ana-edo-anzoategui-ve          | 31700                      |
| Anzoátegui             | Municipio Simón Bolívar                     | mpio-simon-bolivar-edo-anzoategui-ve      | 31800                      |
| Anzoátegui             | Municipio Simón Rodríguez                   | mpio-simon-rodriguez-edo-anzoategui-ve    | 31900                      |
| Anzoátegui             | Municipio Sir Arthur Mc Gregor              | mpio-mcgregor-edo-anzoategui-ve           | 32000                      |
| Anzoátegui             | Municipio Turístico Diego Bautista Urbaneja | mpio-urbaneja-edo-anzoategui-ve           | 32100                      |
| Apure                  | Municipio Achaguas                          | mpio-achaguas-edo-apure-ve                | 40100                      |
| Apure                  | Municipio Biruaca                           | mpio-biruaca-edo-apure-ve                 | 40200                      |
| Apure                  | Municipio Muñoz                             | mpio-munoz-edo-apure-ve                   | 40300                      |
| Apure                  | Municipio Páez                              | mpio-paez-edo-apure-ve                    | 40400                      |
| Apure                  | Municipio Pedro Camejo                      | mpio-pedro-camejo-edo-apure-ve            | 40500                      |
| Apure                  | Municipio Rómulo Gallegos                   | mpio-romulo-gallegos-edo-apure-ve         | 40600                      |
| Apure                  | Municipio San Fernando                      | mpio-san-fernando-edo-apure-ve            | 40700                      |
| Aragua                 | Municipio Bolívar                           | mpio-bolivar-edo-aragua-ve                | 50100                      |
| Aragua                 | Municipio Camatagua                         | mpio-camatagua-edo-aragua-ve              | 50200                      |
| Aragua                 | Municipio Girardot                          | mpio-girardot-edo-aragua-ve               | 50300                      |
| Aragua                 | Municipio José Ángel Lamas                  | mpio-angel-lama-edo-aragua-ve             | 50400                      |
| Aragua                 | Municipio José Félix Ribas                  | mpio-ribas-edo-aragua-ve                  | 50500                      |
| Aragua                 | Municipio José Rafael Revenga               | mpio-revenga-edo-aragua-ve                | 50600                      |
| Aragua                 | Municipio Libertador                        | mpio-libertador-edo-aragua-ve             | 50700                      |
| Aragua                 | Municipio Mario Briceño Iragorry            | mpio-iragorry-edo-aragua-ve               | 50800                      |
| Aragua                 | Municipio San Casimiro                      | mpio-casimiro-edo-aragua-ve               | 50900                      |
| Aragua                 | Municipio San Sebastián                     | mpio-san-sebastian-edo-aragua-ve          | 51000                      |
| Aragua                 | Municipio Santiago Mariño                   | mpio-santiago-marino-edo-aragua-ve        | 51100                      |
| Aragua                 | Municipio Santos Michelena                  | mpio-michelena-edo-aragua-ve              | 51200                      |
| Aragua                 | Municipio Sucre                             | mpio-sucre-edo-aragua-ve                  | 51300                      |
| Aragua                 | Municipio Tovar                             | mpio-tovar-edo-aragua-ve                  | 51400                      |
| Aragua                 | Municipio Urdaneta                          | mpio-arvelo-torrealba-edo-barinas-ve      | 51500                      |
| Aragua                 | Municipio Zamora                            | mpio-arismendi-edo-barinas-ve             | 51600                      |
| Aragua                 | Municipio Francisco Linares Alcántara       | mpio-alcantara-edo-aragua-ve              | 51700                      |
| Aragua                 | Municipio Ocumare de La Costa de Oro        | mpio-ocumare-costa-edo-aragua-ve          | 51800                      |
| Barinas                | Municipio Alberto Arvelo Torrealba          | mpio-arvelo-torrealba-edo-barinas-ve      | 60100                      |
| Barinas                | Municipio Antonio José de Sucre             | mpio-antonio-jose-sucre-edo-barinas-ve    | 60200                      |
| Barinas                | Municipio Arismendi                         | mpio-arismendi-edo-barinas-ve             | 60300                      |
| Barinas                | Municipio Barinas                           | mpio-barinas-edo-barinas-ve               | 60400                      |
| Barinas                | Municipio Bolívar                           | mpio-bolívar-edo-barinas-ve               | 60500                      |
| Barinas                | Municipio Cruz Paredes                      | mpio-paredes-edo-barinas-ve               | 60600                      |
| Barinas                | Municipio Ezequiel Zamora                   | mpio-zamora-edo-barinas-ve                | 60700                      |
| Barinas                | Municipio Obispos                           | mpio-obispos-edo-barinas-ve               | 60800                      |
| Barinas                | Municipio Pedraza                           | mpio-pedraza-edo-barinas-ve               | 60900                      |
| Barinas                | Municipio Rojas                             | mpio-rojas-edo-barinas-ve                 | 61000                      |
| Barinas                | Municipio Sosa                              | mpio-sosa-edo-barinas-ve                  | 61100                      |
| Barinas                | Municipio Andrés Eloy Blanco                | mpio-eloy-blanco-edo-barinas-ve           | 61200                      |
| Bolívar                | Municipio Caroní                            | mpio-caroni-edo-bolivar-ve                | 70100                      |
| Bolívar                | Municipio Cedeño                            | mpio-cedeno-edo-bolivar-ve                | 70200                      |
| Bolívar                | Municipio El Callao                         | mpio-callao-edo-bolivar-ve                | 70300                      |
| Bolívar                | Municipio Gran Sabana                       | mpio-gran-sabana-edo-bolivar-ve           | 70400                      |
| Bolívar                | Municipio Heres                             | mpio-heres-edo-bolivar-ve                 | 70500                      |
| Bolívar                | Municipio Piar                              | mpio-piar-edo-bolivar-ve                  | 70600                      |
| Bolívar                | Municipio Bolivariano Angostura             | mpio-angostura-edo-bolivar-ve             | 70700                      |
| Bolívar                | Municipio Roscio                            | mpio-roscio-edo-bolivar-ve                | 70800                      |
| Bolívar                | Municipio Sifontes                          | mpio-sifonte-edo-bolivar-ve               | 70900                      |
| Bolívar                | Municipio Sucre                             | mpio-sucre-edo-bolivar-ve                 | 71000                      |
| Bolívar                | Municipio Padre Pedro Chien                 | mpio-pedro-chien-edo-bolivar-ve           | 71100                      |
| Carabobo               | Municipio Bejuma                            | mpio-bejuma-edo-carabobo-ve               | 80100                      |
| Carabobo               | Municipio Carlos Arvelo                     | mpio-arvelo-edo-carabobo-ve               | 80200                      |
| Carabobo               | Municipio Diego Ibarra                      | mpio-diego-ibarra-edo-carabobo-ve         | 80300                      |
| Carabobo               | Municipio Guacara                           | mpio-guacara-edo-carabobo-ve              | 80400                      |
| Carabobo               | Municipio Juan José Mora                    | mpio-mora-edo-carabobo-ve                 | 80500                      |
| Carabobo               | Municipio Libertador                        | mpio-libertador-edo-carabobo-ve           | 80600                      |
| Carabobo               | Municipio Los Guayos                        | mpio-guayos-edo-carabobo-ve               | 80700                      |
| Carabobo               | Municipio Miranda                           | mpio-miranda-edo-carabobo-ve              | 80800                      |
| Carabobo               | Municipio Montalbán                         | mpio-montalbán-edo-carabobo-ve            | 80900                      |
| Carabobo               | Municipio Naguanagua                        | mpio-naguanagua-edo-carabobo-ve           | 81000                      |
| Carabobo               | Municipio Puerto Cabello                    | mpio-puerto-cabello-edo-carabobo-ve       | 81100                      |
| Carabobo               | Municipio San Diego                         | mpio-san-diego-edo-carabobo-ve            | 81200                      |
| Carabobo               | Municipio San Joaquín                       | mpio-san-joaquín-edo-carabobo-ve          | 81300                      |
| Carabobo               | Municipio Valencia                          | mpio-valencia-edo-carabobo-ve             | 81400                      |
| Cojedes                | Municipio Anzoátegui                        | mpio-anzoategui-edo-cojedes-ve            | 90100                      |
| Cojedes                | Municipio Tinaquillo                        | mpio-tinaquillo-edo-cojedes-ve            | 90200                      |
| Cojedes                | Municipio Girardot                          | mpio-girardot-edo-cojedes-ve              | 90300                      |
| Cojedes                | Municipio Lima Blanco                       | mpio-lima-blanco-edo-cojedes-ve           | 90400                      |
| Cojedes                | Municipio Pao de San Juan Bautista          | mpio-pao-san-juan-bautista-edo-cojedes-ve | 90500                      |
| Cojedes                | Municipio Ricaurte                          | mpio-ricaurte-edo-cojedes-ve              | 90600                      |
| Cojedes                | Municipio Rómulo Gallegos                   | mpio-gallegos-edo-cojedes-ve              | 90700                      |
| Cojedes                | Municipio Ezequiel Zamora                   | mpio-zamora-edo-cojedes-ve                | 90800                      |
| Cojedes                | Municipio Tinaco                            | mpio-tinaco-edo-cojedes-ve                | 90900                      |
| Falcón                 | Municipio Acosta                            | mpio-acosta-edo-falcon-ve                 | 110100                     |
| Falcón                 | Municipio Bolívar                           | mpio-bolívar-edo-falcon-ve                | 110200                     |
| Falcón                 | Municipio Buchivacoa                        | mpio-buchivacoa-edo-falcon-ve             | 110300                     |
| Falcón                 | Municipio Cacique Manaure                   | mpio-manaure-edo-falcon-ve                | 110400                     |
| Falcón                 | Municipio Carirubana                        | mpio-carirubana-edo-falcon-ve             | 110500                     |
| Falcón                 | Municipio Colina                            | mpio-colina-edo-falcon-ve                 | 110600                     |
| Falcón                 | Municipio Dabajuro                          | mpio-dabajuro-edo-falcon-ve               | 110700                     |
| Falcón                 | Municipio Democracia                        | mpio-democracia-edo-falcon-ve             | 110800                     |
| Falcón                 | Municipio Falcón                            | mpio-falcon-edo-falcon-ve                 | 110900                     |
| Falcón                 | Municipio Federación                        | mpio-federacion-edo-falcon-ve             | 111000                     |
| Falcón                 | Municipio Jacura                            | mpio-jacura-edo-falcon-ve                 | 111100                     |
| Falcón                 | Municipio Los Taques                        | mpio-taques-edo-falcon-ve                 | 111200                     |
| Falcón                 | Municipio Mauroa                            | mpio-mauroa-edo-falcon-ve                 | 111300                     |
| Falcón                 | Municipio Miranda                           | mpio-miranda-edo-falcon-ve                | 111400                     |
| Falcón                 | Municipio Monseñor Iturriza                 | mpio-iturriza-edo-falcon-ve               | 111500                     |
| Falcón                 | Municipio Palmasola                         | mpio-palmasola-edo-falcon-ve              | 111600                     |
| Falcón                 | Municipio Petit                             | mpio-petit-edo-falcon-ve                  | 111700                     |
| Falcón                 | Municipio Píritu                            | mpio-piritu-edo-falcon-ve                 | 111800                     |
| Falcón                 | Municipio San Francisco                     | mpio-san-francisco-edo-falcon-ve          | 111900                     |
| Falcón                 | Municipio Silva                             | mpio-silva-edo-falcon-ve                  | 112000                     |
| Falcón                 | Municipio Sucre                             | mpio-sucre-edo-falcon-ve                  | 112100                     |
| Falcón                 | Municipio Tocópero                          | mpio-tocopero-edo-falcon-ve               | 112200                     |
| Falcón                 | Municipio Unión                             | mpio-union-edo-falcon-ve                  | 112300                     |
| Falcón                 | Municipio Urumaco                           | mpio-urumaco-edo-falcon-ve                | 112400                     |
| Falcón                 | Municipio Zamora                            | mpio-zamora-edo-falcon-ve                 | 112500                     |
| Guárico                | Municipio Camaguán                          | mpio-camaguan-edo-guarico-ve              | 120100                     |
| Guárico                | Municipio Chaguaramas                       | mpio-chaguaramas-edo-guarico-ve           | 120200                     |
| Guárico                | Municipio El Socorro                        | mpio-el-socorro-edo-guarico-ve            | 120300                     |
| Guárico                | Municipio San Gerónimo de Guayabal          | mpio-guayabal-edo-guarico-ve              | 120400                     |
| Guárico                | Municipio Leonardo Infante                  | mpio-leonardo-infante-edo-guarico-ve      | 120500                     |
| Guárico                | Municipio Las Mercedes                      | mpio-las-mercedes-edo-guarico-ve          | 120600                     |
| Guárico                | Municipio Julián Mellado                    | mpio-julian-mellado-edo-guarico-ve        | 120700                     |
| Guárico                | Municipio Francisco de Miranda              | mpio-francisco-miranda-edo-guarico-ve     | 120800                     |
| Guárico                | Municipio José Tadeo Monagas                | mpio-jose-tadeo-monagas-edo-guarico-ve    | 120900                     |
| Guárico                | Municipio Ortiz                             | mpio-ortiz-edo-guarico-ve                 | 121000                     |
| Guárico                | Municipio José Félix Ribas                  | mpio-felix-ribas-edo-guarico-ve           | 121100                     |
| Guárico                | Municipio Juan Germán Roscio                | mpio-german-roscio-edo-guarico-ve         | 121200                     |
| Guárico                | Municipio San José de Guaribe               | mpio-san-jose-guaribe-edo-guarico-ve      | 121300                     |
| Guárico                | Municipio Santa María de Ipire              | mpio-santa-maria-ipire-edo-guarico-ve     | 121400                     |
| Guárico                | Municipio Pedro Zaraza                      | mpio-pedro-zaraza-edo-guarico-ve          | 121500                     |
| Lara                   | Municipio Andrés Eloy Blanco                | mpio-andres-eloy-blanco-edo-lara-ve       | 130100                     |
| Lara                   | Municipio Crespo                            | mpio-crespo-edo-lara-ve                   | 130200                     |
| Lara                   | Municipio Iribarren                         | mpio-iribarren-edo-lara-ve                | 130300                     |
| Lara                   | Municipio Jiménez                           | mpio-jimenez-edo-lara-ve                  | 130400                     |
| Lara                   | Municipio Morán                             | mpio-moran-edo-lara-ve                    | 130500                     |
| Lara                   | Municipio Palavecino                        | mpio-palavecino-edo-lara-ve               | 130600                     |
| Lara                   | Municipio Simón Planas                      | mpio-simon-planas-edo-lara-ve             | 130700                     |
| Lara                   | Municipio Torres                            | mpio-torres-edo-lara-ve                   | 130800                     |
| Lara                   | Municipio Urdaneta                          | mpio-urdaneta-edo-lara-ve                 | 130900                     |
| Mérida                 | Municipio Alberto Adriani                   | mpio-alberto-adriani-edo-merida-ve        | 140100                     |
| Mérida                 | Municipio Andrés Bello                      | mpio-andres-bello-edo-merida-ve           | 140200                     |
| Mérida                 | Municipio Antonio Pinto Salinas             | mpio-pinto-salina-edo-merida-ve           | 140300                     |
| Mérida                 | Municipio Aricagua                          | mpio-aricagua-edo-merida-ve               | 140400                     |
| Mérida                 | Municipio Arzobispo Chacón                  | mpio-chacon-edo-merida-ve                 | 140500                     |
| Mérida                 | Municipio Campo Elías                       | mpio-campo-elias-edo-merida-ve            | 140600                     |
| Mérida                 | Municipio Caracciolo Parra Olmedo           | mpio-parra-olmedo-edo-merida-ve           | 140700                     |
| Mérida                 | Municipio Cardenal Quintero                 | mpio-cardenal-quintero-edo-merida-ve      | 140800                     |
| Mérida                 | Municipio Guaraque                          | mpio-guaraque-edo-merida-ve               | 140900                     |
| Mérida                 | Municipio Julio César Salas                 | mpio-salas-edo-merida-ve                  | 141000                     |
| Mérida                 | Municipio Justo Briceño                     | mpio-justo-briceno-edo-merida-ve          | 141100                     |
| Mérida                 | Municipio Libertador                        | mpio-libertador-edo-merida-ve             | 141200                     |
| Mérida                 | Municipio Miranda                           | mpio-miranda-edo-merida-ve                | 141300                     |
| Mérida                 | Municipio Obispo Ramos de Lora              | mpio-ramos-lora-edo-merida-ve             | 141400                     |
| Mérida                 | Municipio Padre Noguera                     | mpio-padre-noguera-edo-merida-ve          | 141500                     |
| Mérida                 | Municipio Pueblo Llano                      | mpio-pueblo-llano-edo-merida-ve           | 141600                     |
| Mérida                 | Municipio Rangel                            | mpio-rangel-edo-merida-ve                 | 141700                     |
| Mérida                 | Municipio Rivas Dávila                      | mpio-rivas-davila-edo-merida-ve           | 141800                     |
| Mérida                 | Municipio Santos Marquina                   | mpio-santos-marquina-edo-merida-ve        | 141900                     |
| Mérida                 | Municipio Sucre                             | mpio-sucre-edo-merida-ve                  | 142000                     |
| Mérida                 | Municipio Tovar                             | mpio-tovar-edo-merida-ve                  | 142100                     |
| Mérida                 | Municipio Tulio Febres Cordero              | mpio-febres-edo-merida-ve                 | 142200                     |
| Mérida                 | Municipio Zea                               | mpio-zea-edo-merida-ve                    | 142300                     |
| Miranda                | Municipio Acevedo                           | mpio-acevedo-edo-miranda-ve               | 150100                     |
| Miranda                | Municipio Andrés Bello                      | mpio-andres-bello-edo-miranda-ve          | 150200                     |
| Miranda                | Municipio Baruta                            | mpio-baruta-edo-miranda-ve                | 150300                     |
| Miranda                | Municipio Brión                             | mpio-brion-edo-miranda-ve                 | 150400                     |
| Miranda                | Municipio Buroz                             | mpio-buroz-edo-miranda-ve                 | 150500                     |
| Miranda                | Municipio Carrizal                          | mpio-carrizal-edo-miranda-ve              | 150600                     |
| Miranda                | Municipio Chacao                            | mpio-chacao-edo-miranda-ve                | 150700                     |
| Miranda                | Municipio Cristóbal Rojas                   | mpio-cristobal-rojas-edo-miranda-ve       | 150800                     |
| Miranda                | Municipio El Hatillo                        | mpio-hatillo-edo-miranda-ve               | 150900                     |
| Miranda                | Municipio Bolivariano Guaicaipuro           | mpio-guaicaipuro-edo-miranda-ve           | 151000                     |
| Miranda                | Municipio Independencia                     | mpio-independencia-edo-miranda-ve         | 151100                     |
| Miranda                | Municipio Lander                            | mpio-lander-edo-miranda-ve                | 151200                     |
| Miranda                | Municipio Los Salias                        | mpio-salias-edo-miranda-ve                | 151300                     |
| Miranda                | Municipio Páez                              | mpio-paez-edo-miranda-ve                  | 151400                     |
| Miranda                | Municipio Paz Castillo                      | mpio-paz-castillo-edo-miranda-ve          | 151500                     |
| Miranda                | Municipio Pedro Gual                        | mpio-pedro-gual-edo-miranda-ve            | 151600                     |
| Miranda                | Municipio Plaza                             | mpio-plaza-edo-miranda-ve                 | 151700                     |
| Miranda                | Municipio Simón Bolívar                     | mpio-simon-bolivar-edo-miranda-ve         | 151800                     |
| Miranda                | Municipio Sucre                             | mpio-sucre-edo-miranda-ve                 | 151900                     |
| Miranda                | Municipio Urdaneta                          | mpio-urdaneta-edo-miranda-ve              | 152000                     |
| Miranda                | Municipio Zamora                            | mpio-zamora-edo-miranda-ve                | 152100                     |
| Monagas                | Municipio Acosta                            | mpio-acosta-edo-monagas-ve                | 160100                     |
| Monagas                | Municipio Aguasay                           | mpio-aguasay-edo-monagas-ve               | 160200                     |
| Monagas                | Municipio Bolívar                           | mpio-bolivar-edo-monagas-ve               | 160300                     |
| Monagas                | Municipio Caripe                            | mpio-caripe-edo-monagas-ve                | 160400                     |
| Monagas                | Municipio Cedeño                            | mpio-cedeno-edo-monagas-ve                | 160500                     |
| Monagas                | Municipio Ezequiel Zamora                   | mpio-zamora-edo-monagas-ve                | 160600                     |
| Monagas                | Municipio Libertador                        | mpio-libertador-edo-monagas-ve            | 160700                     |
| Monagas                | Municipio Maturín                           | mpio-maturin-edo-monagas-ve               | 160800                     |
| Monagas                | Municipio Piar                              | mpio-piar-edo-monagas-ve                  | 160900                     |
| Monagas                | Municipio Punceres                          | mpio-punceres-edo-monagas-ve              | 161000                     |
| Monagas                | Municipio Santa Bárbara                     | mpio-santa-barbara-edo-monagas-ve         | 161100                     |
| Monagas                | Municipio Sotillo                           | mpio-sotillo-edo-monagas-ve               | 161200                     |
| Monagas                | Municipio Uracoa                            | mpio-uracoa-edo-monagas-ve                | 161300                     |
| Nueva Esparta          | Municipio Antolín del Campo                 | mpio-antolin-edo-nva-esparta-ve           | 170100                     |
| Nueva Esparta          | Municipio Arismendi                         | mpio-arismendi-edo-nva-esparta-ve         | 170200                     |
| Nueva Esparta          | Municipio Díaz                              | mpio-diaz-edo-nva-esparta-ve              | 170300                     |
| Nueva Esparta          | Municipio García                            | mpio-garcia-edo-nva-esparta-ve            | 170400                     |
| Nueva Esparta          | Municipio Gómez                             | mpio-gomez-edo-nva-esparta-ve             | 170500                     |
| Nueva Esparta          | Municipio Maneiro                           | mpio-maneiro-edo-nva-esparta-ve           | 170600                     |
| Nueva Esparta          | Municipio Marcano                           | mpio-marcano-edo-nva-esparta-ve           | 170700                     |
| Nueva Esparta          | Municipio Mariño                            | mpio-marino-edo-nva-esparta-ve            | 170800                     |
| Nueva Esparta          | Municipio Península de Macanao              | mpio-macanao-edo-nva-esparta-ve           | 170900                     |
| Nueva Esparta          | Municipio Tubores                           | mpio-tubores-edo-nva-esparta-ve           | 171000                     |
| Nueva Esparta          | Municipio Villalba                          | mpio-villalba-edo-nva-esparta-ve          | 171100                     |
| Portuguesa             | Municipio Agua Blanca                       | mpio-agua-blanca-edo-portuguesa-ve        | 180100                     |
| Portuguesa             | Municipio Araure                            | mpio-araure-edo-portuguesa-ve             | 180200                     |
| Portuguesa             | Municipio Esteller                          | mpio-esteller-edo-portuguesa-ve           | 180300                     |
| Portuguesa             | Municipio Guanare                           | mpio-guanare-edo-portuguesa-ve            | 180400                     |
| Portuguesa             | Municipio Guanarito                         | mpio-guanarito-edo-portuguesa-ve          | 180500                     |
| Portuguesa             | Municipio Monseñor José Vicente de Unda     | mpio-vicente-unda-edo-portuguesa-ve       | 180600                     |
| Portuguesa             | Municipio Ospino                            | mpio-ospino-edo-portuguesa-ve             | 180700                     |
| Portuguesa             | Municipio Páez                              | mpio-paez-edo-portuguesa-ve               | 180800                     |
| Portuguesa             | Municipio Papelón                           | mpio-papelon-edo-portuguesa-ve            | 180900                     |
| Portuguesa             | Municipio San Genaro de Boconoito           | mpio-boconoito-edo-portuguesa-ve          | 181000                     |
| Portuguesa             | Municipio San Rafael de Onoto               | mpio-onoto-edo-portuguesa-ve              | 181100                     |
| Portuguesa             | Municipio Santa Rosalía                     | mpio-santa-rosalia-edo-portuguesa-ve      | 181200                     |
| Portuguesa             | Municipio Sucre                             | mpio-sucre-edo-portuguesa-ve              | 181300                     |
| Portuguesa             | Municipio Turén                             | mpio-turen-edo-portuguesa-ve              | 181400                     |
| Sucre                  | Municipio Andrés Eloy Blanco                | mpio-andres-eloy-blanco-edo-sucre-ve      | 190100                     |
| Sucre                  | Municipio Andrés Mata                       | mpio-andres-mata-edo-sucre-ve             | 190200                     |
| Sucre                  | Municipio Arismendi                         | mpio-arismendi-edo-sucre-ve               | 190300                     |
| Sucre                  | Municipio Benítez                           | mpio-benitez-edo-sucre-ve                 | 190400                     |
| Sucre                  | Municipio Bermúdez                          | mpio-bermudez-edo-sucre-ve                | 190500                     |
| Sucre                  | Municipio Bolívar                           | mpio-bolivar-edo-sucre-ve                 | 190600                     |
| Sucre                  | Municipio Cajigal                           | mpio-cajigal-edo-sucre-ve                 | 190700                     |
| Sucre                  | Municipio Cruz Salmerón Acosta              | mpio-cruz-salmeron-edo-sucre-ve           | 190800                     |
| Sucre                  | Municipio Libertador                        | mpio-libertador-edo-sucre-ve              | 190900                     |
| Sucre                  | Municipio Mariño                            | mpio-marino-edo-sucre-ve                  | 191000                     |
| Sucre                  | Municipio Mejía                             | mpio-mejia-edo-sucre-ve                   | 191100                     |
| Sucre                  | Municipio Montes                            | mpio-montes                               | 191200                     |
| Sucre                  | Municipio Ribero                            | mpio-ribero-edo-sucre-ve                  | 191300                     |
| Sucre                  | Municipio Sucre                             | mpio-sucre-edo-sucre-ve                   | 191400                     |
| Sucre                  | Municipio Valdez                            | mpio-valdez-edo-sucre-ve                  | 191500                     |
| Táchira                | Municipio Andrés Bello                      | mpio-andres-bello-edo-tachira-ve          | 200100                     |
| Táchira                | Municipio Antonio Rómulo Costa              | mpio-antonio-costa-edo-tachira-ve         | 200200                     |
| Táchira                | Municipio Ayacucho                          | mpio-ayacucho-edo-tachira-ve              | 200300                     |
| Táchira                | Municipio Bolívar                           | mpio-bolívar-edo-tachira-ve               | 200400                     |
| Táchira                | Municipio Cárdenas                          | mpio-cardenas-edo-tachira-ve              | 200500                     |
| Táchira                | Municipio Córdoba                           | mpio-cordoba-edo-tachira-ve               | 200600                     |
| Táchira                | Municipio Fernández Feo                     | mpio-fernandez-feo-edo-tachira-ve         | 200700                     |
| Táchira                | Municipio Francisco de Miranda              | mpio-francisco-miranda-edo-tachira-ve     | 200800                     |
| Táchira                | Municipio García de Hevia                   | mpio-garcia-hevia-edo-tachira-ve          | 200900                     |
| Táchira                | Municipio Guásimos                          | mpio-guasimos-edo-tachira-ve              | 201000                     |
| Táchira                | Municipio Independencia                     | mpio-independencia-edo-tachira-ve         | 201100                     |
| Táchira                | Municipio Jáuregui                          | mpio-jauregui-edo-tachira-ve              | 201200                     |
| Táchira                | Municipio José María Vargas                 | mpio-jose-vargas-edo-tachira-ve           | 201300                     |
| Táchira                | Municipio Junín                             | mpio-junin-edo-tachira-ve                 | 201400                     |
| Táchira                | Municipio Libertad                          | mpio-libertad-edo-tachira-ve              | 201500                     |
| Táchira                | Municipio Libertador                        | mpio-libertador-edo-tachira-ve            | 201600                     |
| Táchira                | Municipio Lobatera                          | mpio-lobatera-edo-tachira-ve              | 201700                     |
| Táchira                | Municipio Michelena                         | mpio-michelena-edo-tachira-ve             | 201800                     |
| Táchira                | Municipio Panamericano                      | mpio-panamericano-edo-tachira-ve          | 201900                     |
| Táchira                | Municipio Pedro María Ureña                 | mpio-pedro-ureña-edo-tachira-ve           | 202000                     |
| Táchira                | Municipio Rafael Urdaneta                   | mpio-rafael-urdaneta-edo-tachira-ve       | 202100                     |
| Táchira                | Municipio Samuel Darío Maldonado            | mpio-samuel-maldonado-edo-tachira-ve      | 202200                     |
| Táchira                | Municipio San Cristóbal                     | mpio-san-cristóbal-edo-tachira-ve         | 202300                     |
| Táchira                | Municipio Seboruco                          | mpio-seboruco-edo-tachira-ve              | 202400                     |
| Táchira                | Municipio Simón Rodríguez                   | mpio-simon-rodríguez-edo-tachira-ve       | 202500                     |
| Táchira                | Municipio Sucre                             | mpio-sucre-edo-tachira-ve                 | 202600                     |
| Táchira                | Municipio Torbes                            | mpio-torbes-edo-tachira-ve                | 202700                     |
| Táchira                | Municipio Uribante                          | mpio-uribante-edo-tachira-ve              | 202800                     |
| Táchira                | Municipio San Judas Tadeo                   | mpio-judas-tadeo-edo-tachira-ve           | 202900                     |
| Trujillo               | Municipio Andrés Bello                      | mpio-andres-bello-edo-trujillo-ve         | 210100                     |
| Trujillo               | Municipio Boconó                            | mpio-bocono-edo-trujillo-ve               | 210200                     |
| Trujillo               | Municipio Bolívar                           | mpio-bolivar-edo-trujillo-ve              | 210300                     |
| Trujillo               | Municipio Candelaria                        | mpio-candelaria-edo-trujillo-ve           | 210400                     |
| Trujillo               | Municipio Carache                           | mpio-carache-edo-trujillo-ve              | 210500                     |
| Trujillo               | Municipio Escuque                           | mpio-escuque-edo-trujillo-ve              | 210600                     |
| Trujillo               | Municipio José Felipe Márquez Cañizales     | mpio-canizales-edo-trujillo-ve            | 210700                     |
| Trujillo               | Municipio Juan Vicente Campo Elías          | mpio-campo-elias-edo-trujillo-ve          | 210800                     |
| Trujillo               | Municipio La Ceiba                          | mpio-la-ceiba-edo-trujillo-ve             | 210900                     |
| Trujillo               | Municipio Miranda                           | mpio-miranda-edo-trujillo-ve              | 211000                     |
| Trujillo               | Municipio Monte Carmelo                     | mpio-monte-carmelo-edo-trujillo-ve        | 211100                     |
| Trujillo               | Municipio Motatán                           | mpio-motatán-edo-trujillo-ve              | 211200                     |
| Trujillo               | Municipio Pampán                            | mpio-pampan-edo-trujillo-ve               | 211300                     |
| Trujillo               | Municipio Pampanito                         | mpio-pampanito-edo-trujillo-ve            | 211400                     |
| Trujillo               | Municipio Rafael Rangel                     | mpio-rafael-rangel-edo-trujillo-ve        | 211500                     |
| Trujillo               | Municipio San Rafael de Carvajal            | mpio-san-rafael-edo-trujillo-ve           | 211600                     |
| Trujillo               | Municipio Sucre                             | mpio-sucre-edo-trujillo-ve                | 211700                     |
| Trujillo               | Municipio Trujillo                          | mpio-trujillo-edo-trujillo-ve             | 211800                     |
| Trujillo               | Municipio Urdaneta                          | mpio-urdaneta-edo-trujillo-ve             | 211900                     |
| Trujillo               | Municipio Valera                            | mpio-valera-edo-trujillo-ve               | 212000                     |
| Yaracuy                | Municipio Arístides Bastidas                | mpio-aristedes-bastidas-edo-yaracuy-ve    | 220100                     |
| Yaracuy                | Municipio Bolívar                           | mpio-bolivar-edo-yaracuy-ve               | 220200                     |
| Yaracuy                | Municipio Bruzual                           | mpio-bruzual-edo-yaracuy-ve               | 220300                     |
| Yaracuy                | Municipio Cocorote                          | mpio-cocorote-edo-yaracuy-ve              | 220400                     |
| Yaracuy                | Municipio Independencia                     | mpio-independencia-edo-yaracuy-ve         | 220500                     |
| Yaracuy                | Municipio José Antonio Páez                 | mpio-paez-edo-yaracuy-ve                  | 220600                     |
| Yaracuy                | Municipio La Trinidad                       | mpio-trinidad-edo-yaracuy-ve              | 220700                     |
| Yaracuy                | Municipio Manuel Monge                      | mpio-manuel-monge-edo-yaracuy-ve          | 220800                     |
| Yaracuy                | Municipio Nirgua                            | mpio-nirgua-edo-yaracuy-ve                | 220900                     |
| Yaracuy                | Municipio Peña                              | mpio-pena-edo-yaracuy-ve                  | 221000                     |
| Yaracuy                | Municipio San Felipe                        | mpio-san-felipe-edo-yaracuy-ve            | 221100                     |
| Yaracuy                | Municipio Sucre                             | mpio-sucre-edo-yaracuy-ve                 | 221200                     |
| Yaracuy                | Municipio Urachiche                         | mpio-urachiche-edo-yaracuy-ve             | 221300                     |
| Yaracuy                | Municipio Veroes                            | mpio-veroes-edo-yaracuy-ve                | 221400                     |
| Zulia                  | Municipio Almirante Padilla                 | mpio-almirante-padilla-edo-zulia-ve       | 230100                     |
| Zulia                  | Municipio Baralt                            | mpio-baralt-edo-zulia-ve                  | 230200                     |
| Zulia                  | Municipio Cabimas                           | mpio-cabimas-edo-zulia-ve                 | 230300                     |
| Zulia                  | Municipio Catatumbo                         | mpio-catatumbo-edo-zulia-ve               | 230400                     |
| Zulia                  | Municipio Colón                             | mpio-colon-edo-zulia-ve                   | 230500                     |
| Zulia                  | Municipio Francisco Javier Pulgar           | mpio-pulgar-edo-zulia-ve                  | 230600                     |
| Zulia                  | Municipio Jesús Enrique Lossada             | mpio-lossada-edo-zulia-ve                 | 230700                     |
| Zulia                  | Municipio Jesús María Semprún               | mpio-semprun-edo-zulia-ve                 | 230800                     |
| Zulia                  | Municipio La Cañada de Urdaneta             | mpio-canada-urdaneta-edo-zulia-ve         | 230900                     |
| Zulia                  | Municipio Lagunillas                        | mpio-lagunillas-edo-zulia-ve              | 231000                     |
| Zulia                  | Municipio Machiques de Perijá               | mpio-machiques-perija-edo-zulia-ve        | 231100                     |
| Zulia                  | Municipio Mara                              | mpio-mara-edo-zulia-ve                    | 231200                     |
| Zulia                  | Municipio Maracaibo                         | mpio-maracaibo-edo-zulia-ve               | 231300                     |
| Zulia                  | Municipio Miranda                           | mpio-miranda-edo-zulia-ve                 | 231400                     |
| Zulia                  | Municipio Indígena Bolivariano Guajira      | mpio-guajira-edo-zulia-ve                 | 231500                     |
| Zulia                  | Municipio Rosario de Perijá                 | mpio-rosario-perija-edo-zulia-ve          | 231600                     |
| Zulia                  | Municipio San Francisco                     | mpio-san-francisco-edo-zulia-ve           | 231700                     |
| Zulia                  | Municipio Santa Rita                        | mpio-santa-rita-edo-zulia-ve              | 231800                     |
| Zulia                  | Municipio Simón Bolívar                     | mpio-simon-bolívar-edo-zulia-ve           | 231900                     |
| Zulia                  | Municipio Sucre                             | mpio-sucre-edo-zulia-ve                   | 232000                     |
| Zulia                  | Municipio Valmore Rodríguez                 | mpio-valmore-rodríguez-edo-zulia-ve       | 232100                     |
| Dependencias Federales | Dependencia Federales                       | mpio-dependencia-federales-ve             | 250100                     |
| Vargas                 | Municipio Vargas                            | mpio-vargas-edo-vargas-ve                 | 240100                     |
| Delta Amacuro          | Municipio Antonio Díaz                      | mpio-antonio-diaz-edo-delta-amacuro-ve    | 100100                     |
| Delta Amacuro          | Municipio Casacoima                         | mpio-casacoima-edo-delta-amacuro-ve       | 100200                     |
| Delta Amacuro          | Municipio Pedernales                        | mpio-pedernales-edo-delta-amacuro-ve      | 100300                     |
| Delta Amacuro          | Municipio Tucupita                          | mpio-tucupita-edo-delta-amacuro-ve        | 100400                     |
| Amazonas               | Municipio Autónomo Alto Orinoco             | mpio-alto-orinoco-edo-amazonas-ve         | 20100                      |
| Amazonas               | Municipio Autónomo Atabapo                  | mpio-atabapo-edo-amazonas-ve              | 20200                      |
| Amazonas               | Municipio Autónomo Atures                   | mpio-atures-edo-amazonas-ve               | 20300                      |
| Amazonas               | Municipio Autónomo Autana                   | mpio-autana-edo-amazonas-ve               | 20400                      |
| Amazonas               | Municipio Autónomo Maroa                    | mpio-maroa-edo-amazonas-ve                | 20500                      |
| Amazonas               | Municipio Autónomo Manapiare                | mpio-manapiare-edo-amazonas-ve            | 20600                      |
| Amazonas               | Municipio Autónomo Río Negro                | mpio-rio-negro-edo-amazonas-ve            | 20700                      