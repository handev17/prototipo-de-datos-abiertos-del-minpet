| Sectores              | Identificador         |
|-----------------------|-----------------------|
| Educación             | educacion             |
| Salud                 | salud                 |
| Transporte            | transporte            |
| Medio ambiente        | medio-ambiente        |
| Sanidad y desechos    | saneamiento-residuos  |
| Gobernabilidad        | gobernabilidad        |
| Prosperidad económica | prosperidad-economica |
| Alimentación y agua   | alimentacion-agua     |
| Asentamientos humanos | asentamientos-humanos |
| Desarrollo social     | desarrollo-social     |
| Energía               | energia               |