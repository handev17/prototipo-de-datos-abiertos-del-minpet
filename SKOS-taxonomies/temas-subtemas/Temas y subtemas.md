| Tema                  | Subtema-objetivo                                      |
|-----------------------|-------------------------------------------------------|
| Educación             | Acceso a la educación                                 |
| Educación             | Información y toma de decisiones en  educación        |
| Educación             | Inclusion en educación                                |
| Salud                 | Incrementando el acceso a la salud                    |
| Salud                 | calidad de la provisio del servicio de  salud         |
| Salud                 | Prevencion de la difusion de enfermedades             |
| Transporte            | Acceso al transporte                                  |
| Transporte            | Eficiencia del transporte                             |
| Transporte            | Seguridad del transporte                              |
| Medio ambiente        | Preservación del medio ambiente                       |
| Medio ambiente        | Resistencia al cambio climatico y desastres naturales |
| Medio ambiente        | Sostenibilidad y contaminación                        |
| Sanidad y desechos    | Acceso a sanidad                                      |
| Sanidad y desechos    | Manejo de desperdicios                                |
| Sanidad y desechos    | Reciclaje                                             |
| Gobernabilidad        | Compromiso ciudadano y respuesta                      |
| Gobernabilidad        | Transparencia y contabilidad                          |
| Gobernabilidad        | Crimen y Violencia                                    |
| Prosperidad económica | Innovación y emprendimiento.                          |
| Prosperidad económica | empleabilidad y desempleo                             |
| Prosperidad económica | Comercio e inversión                                  |
| Alimentación y agua   | acceso a comida saludable y económica                 |
| Alimentación y agua   | acceso a agua limpia                                  |
| Alimentación y agua   | agricultura sostenible                                |
| Asentamientos humanos | Habilidad para alojar ciudadanos                      |
| Asentamientos humanos | Habilidad para manejar urbanizaciones                 |
| Asentamientos humanos | acceso a infraestructura                              |
| Desarrollo social     | Igualdad social                                       |
| Desarrollo social     | Igualdad de genero                                    |
| Desarrollo social     | Seguridad financiera y social                         |
| Energía               | acceso a electricidad                                 |
| Energía               | Eficiencia en la provisión del servicio eléctrico     |
| Energía               | Confiabilidad de la energía en los hogares            |