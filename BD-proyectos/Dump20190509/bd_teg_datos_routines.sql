-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bd_teg_datos
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `view_proyectos_dates`
--

DROP TABLE IF EXISTS `view_proyectos_dates`;
/*!50001 DROP VIEW IF EXISTS `view_proyectos_dates`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `view_proyectos_dates` AS SELECT 
 1 AS `id_proyecto`,
 1 AS `id_organizacion`,
 1 AS `id_tipo_org`,
 1 AS `id_ente_rector`,
 1 AS `param_enterector`,
 1 AS `enterector`,
 1 AS `param_tipo_org`,
 1 AS `tipo_org`,
 1 AS `param_organizacion`,
 1 AS `organizacion`,
 1 AS `publicacion`,
 1 AS `proyecto`,
 1 AS `descripcion`,
 1 AS `fecha_inicio`,
 1 AS `fecha_fin`,
 1 AS `avance_fisico`,
 1 AS `avance_financiero`,
 1 AS `monto_total_proy`,
 1 AS `aprobado_bs`,
 1 AS `ejecutado_bs`,
 1 AS `url`,
 1 AS `referencia_pag`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_proyectos`
--

DROP TABLE IF EXISTS `view_proyectos`;
/*!50001 DROP VIEW IF EXISTS `view_proyectos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8mb4;
/*!50001 CREATE VIEW `view_proyectos` AS SELECT 
 1 AS `id_proyecto`,
 1 AS `id_organizacion`,
 1 AS `id_tipo_org`,
 1 AS `id_ente_rector`,
 1 AS `param_enterector`,
 1 AS `enterector`,
 1 AS `param_tipo_org`,
 1 AS `tipo_org`,
 1 AS `param_organizacion`,
 1 AS `organizacion`,
 1 AS `publicacion`,
 1 AS `proyecto`,
 1 AS `descripcion`,
 1 AS `fecha_inicio`,
 1 AS `fecha_fin`,
 1 AS `avance_fisico`,
 1 AS `avance_financiero`,
 1 AS `monto_total_proy`,
 1 AS `aprobado_bs`,
 1 AS `ejecutado_bs`,
 1 AS `url`,
 1 AS `referencia_pag`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_proyectos_dates`
--

/*!50001 DROP VIEW IF EXISTS `view_proyectos_dates`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`teg_user`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_proyectos_dates` AS select `rendicion`.`id_proyecto` AS `id_proyecto`,`rendicion`.`id_organizacion` AS `id_organizacion`,`rendicion`.`id_tipo_org` AS `id_tipo_org`,`rendicion`.`id_ente_rector` AS `id_ente_rector`,`rendicion`.`param_enterector` AS `param_enterector`,`rendicion`.`enterector` AS `enterector`,`rendicion`.`param_tipo_org` AS `param_tipo_org`,`rendicion`.`tipo_org` AS `tipo_org`,`rendicion`.`param_organizacion` AS `param_organizacion`,`rendicion`.`organizacion` AS `organizacion`,`rendicion`.`publicacion` AS `publicacion`,`rendicion`.`proyecto` AS `proyecto`,`rendicion`.`descripcion` AS `descripcion`,cast(`rendicion`.`fecha_inicio` as date) AS `fecha_inicio`,cast(`rendicion`.`fecha_fin` as date) AS `fecha_fin`,`rendicion`.`avance_fisico` AS `avance_fisico`,`rendicion`.`avance_financiero` AS `avance_financiero`,`rendicion`.`monto_total_proy` AS `monto_total_proy`,`rendicion`.`aprobado_bs` AS `aprobado_bs`,`rendicion`.`ejecutado_bs` AS `ejecutado_bs`,`fuentes`.`url` AS `url`,`fuentes`.`referencia_pag` AS `referencia_pag` from ((select `orga`.`id_organizacion` AS `id_organizacion`,`orga`.`id_tipo_org` AS `id_tipo_org`,`orga`.`id_ente_rector` AS `id_ente_rector`,`orga`.`param_enterector` AS `param_enterector`,`orga`.`enterector` AS `enterector`,`orga`.`param_tipo_org` AS `param_tipo_org`,`orga`.`tipo_org` AS `tipo_org`,`orga`.`param_organizacion` AS `param_organizacion`,`orga`.`organizacion` AS `organizacion`,`proy`.`id_proyecto` AS `id_proyecto`,`proy`.`publicacion` AS `publicacion`,`proy`.`proyecto` AS `proyecto`,`proy`.`descripcion` AS `descripcion`,`proy`.`fecha_inicio` AS `fecha_inicio`,`proy`.`fecha_fin` AS `fecha_fin`,`proy`.`avance_fisico` AS `avance_fisico`,`proy`.`avance_financiero` AS `avance_financiero`,`proy`.`monto_total_proy` AS `monto_total_proy`,`proy`.`aprobado_bs` AS `aprobado_bs`,`proy`.`ejecutado_bs` AS `ejecutado_bs`,`proy`.`id_fuente_URL` AS `id_fuente_URL` from ((select `orgtiposorg`.`id_organizacion` AS `id_organizacion`,`orgtiposorg`.`id_tipo_org` AS `id_tipo_org`,`entesrect`.`id_ente_rector` AS `id_ente_rector`,`entesrect`.`identificador_alt` AS `param_enterector`,`entesrect`.`nombre` AS `enterector`,`orgtiposorg`.`alttiporg` AS `param_tipo_org`,`orgtiposorg`.`tipo_org` AS `tipo_org`,`orgtiposorg`.`altorg` AS `param_organizacion`,`orgtiposorg`.`nombre` AS `organizacion` from (`tbl_entes_rectores` `entesrect` join (select `org`.`id_organizacion` AS `id_organizacion`,`org`.`id_tipo_org` AS `id_tipo_org`,`org`.`id_ente_rector` AS `id_ente_rector`,`org`.`identificador_alt` AS `altorg`,`org`.`nombre` AS `nombre`,`tiposorg`.`identificador_alt` AS `alttiporg`,`tiposorg`.`tipo_org` AS `tipo_org` from (`tbl_organizaciones` `org` join `tbl_tipos_org` `tiposorg` on((`org`.`id_tipo_org` = `tiposorg`.`id_tipo_org`)))) `orgtiposorg` on((`entesrect`.`id_ente_rector` = `orgtiposorg`.`id_ente_rector`)))) `orga` join `tbl_proyectos` `proy` on((`orga`.`id_organizacion` = `proy`.`id_organizacion`)))) `rendicion` join `tbl_fuentesurl` `fuentes` on((`rendicion`.`id_fuente_URL` = `fuentes`.`id_fuente_URL`))) where (`rendicion`.`id_proyecto` not in (115,133,134,135,136,204,205,206,207,212,213)) order by `rendicion`.`id_proyecto` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_proyectos`
--

/*!50001 DROP VIEW IF EXISTS `view_proyectos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`teg_user`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_proyectos` AS select `rendicion`.`id_proyecto` AS `id_proyecto`,`rendicion`.`id_organizacion` AS `id_organizacion`,`rendicion`.`id_tipo_org` AS `id_tipo_org`,`rendicion`.`id_ente_rector` AS `id_ente_rector`,`rendicion`.`param_enterector` AS `param_enterector`,`rendicion`.`enterector` AS `enterector`,`rendicion`.`param_tipo_org` AS `param_tipo_org`,`rendicion`.`tipo_org` AS `tipo_org`,`rendicion`.`param_organizacion` AS `param_organizacion`,`rendicion`.`organizacion` AS `organizacion`,`rendicion`.`publicacion` AS `publicacion`,`rendicion`.`proyecto` AS `proyecto`,`rendicion`.`descripcion` AS `descripcion`,`rendicion`.`fecha_inicio` AS `fecha_inicio`,`rendicion`.`fecha_fin` AS `fecha_fin`,`rendicion`.`avance_fisico` AS `avance_fisico`,`rendicion`.`avance_financiero` AS `avance_financiero`,`rendicion`.`monto_total_proy` AS `monto_total_proy`,`rendicion`.`aprobado_bs` AS `aprobado_bs`,`rendicion`.`ejecutado_bs` AS `ejecutado_bs`,`fuentes`.`url` AS `url`,`fuentes`.`referencia_pag` AS `referencia_pag` from ((select `orga`.`id_organizacion` AS `id_organizacion`,`orga`.`id_tipo_org` AS `id_tipo_org`,`orga`.`id_ente_rector` AS `id_ente_rector`,`orga`.`param_enterector` AS `param_enterector`,`orga`.`enterector` AS `enterector`,`orga`.`param_tipo_org` AS `param_tipo_org`,`orga`.`tipo_org` AS `tipo_org`,`orga`.`param_organizacion` AS `param_organizacion`,`orga`.`organizacion` AS `organizacion`,`proy`.`id_proyecto` AS `id_proyecto`,`proy`.`publicacion` AS `publicacion`,`proy`.`proyecto` AS `proyecto`,`proy`.`descripcion` AS `descripcion`,`proy`.`fecha_inicio` AS `fecha_inicio`,`proy`.`fecha_fin` AS `fecha_fin`,`proy`.`avance_fisico` AS `avance_fisico`,`proy`.`avance_financiero` AS `avance_financiero`,`proy`.`monto_total_proy` AS `monto_total_proy`,`proy`.`aprobado_bs` AS `aprobado_bs`,`proy`.`ejecutado_bs` AS `ejecutado_bs`,`proy`.`id_fuente_URL` AS `id_fuente_URL` from ((select `orgtiposorg`.`id_organizacion` AS `id_organizacion`,`orgtiposorg`.`id_tipo_org` AS `id_tipo_org`,`entesrect`.`id_ente_rector` AS `id_ente_rector`,`entesrect`.`identificador_alt` AS `param_enterector`,`entesrect`.`nombre` AS `enterector`,`orgtiposorg`.`alttiporg` AS `param_tipo_org`,`orgtiposorg`.`tipo_org` AS `tipo_org`,`orgtiposorg`.`altorg` AS `param_organizacion`,`orgtiposorg`.`nombre` AS `organizacion` from (`tbl_entes_rectores` `entesrect` join (select `org`.`id_organizacion` AS `id_organizacion`,`org`.`id_tipo_org` AS `id_tipo_org`,`org`.`id_ente_rector` AS `id_ente_rector`,`org`.`identificador_alt` AS `altorg`,`org`.`nombre` AS `nombre`,`tiposorg`.`identificador_alt` AS `alttiporg`,`tiposorg`.`tipo_org` AS `tipo_org` from (`tbl_organizaciones` `org` join `tbl_tipos_org` `tiposorg` on((`org`.`id_tipo_org` = `tiposorg`.`id_tipo_org`)))) `orgtiposorg` on((`entesrect`.`id_ente_rector` = `orgtiposorg`.`id_ente_rector`)))) `orga` join `tbl_proyectos` `proy` on((`orga`.`id_organizacion` = `proy`.`id_organizacion`)))) `rendicion` join `tbl_fuentesurl` `fuentes` on((`rendicion`.`id_fuente_URL` = `fuentes`.`id_fuente_URL`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'bd_teg_datos'
--

--
-- Dumping routines for database 'bd_teg_datos'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-09 12:59:04
