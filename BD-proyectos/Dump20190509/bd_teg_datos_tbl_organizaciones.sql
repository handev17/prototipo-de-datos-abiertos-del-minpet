-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: bd_teg_datos
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_organizaciones`
--

DROP TABLE IF EXISTS `tbl_organizaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbl_organizaciones` (
  `id_organizacion` int(11) NOT NULL,
  `id_tipo_org` int(11) DEFAULT NULL,
  `id_ente_rector` int(11) DEFAULT NULL,
  `identificador_alt` text,
  `nombre` text,
  PRIMARY KEY (`id_organizacion`),
  KEY `fk_id_ente_rector_idx` (`id_ente_rector`),
  KEY `fk_id_tipo_org_idx` (`id_tipo_org`),
  CONSTRAINT `fk_id_ente_rector` FOREIGN KEY (`id_ente_rector`) REFERENCES `tbl_entes_rectores` (`id_ente_rector`),
  CONSTRAINT `fk_id_tipo_org` FOREIGN KEY (`id_tipo_org`) REFERENCES `tbl_tipos_org` (`id_tipo_org`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_organizaciones`
--

LOCK TABLES `tbl_organizaciones` WRITE;
/*!40000 ALTER TABLE `tbl_organizaciones` DISABLE KEYS */;
INSERT INTO `tbl_organizaciones` VALUES (1,1,1,'mpetromin','MINISTERIO DEL PODER POPULAR DE PETRÓLEO Y MINERÍA'),(2,2,1,'bielovenezolana','PETROLERA BIELOVENEZOLANA.SA'),(3,2,1,'gasGuarico','GAS GUÁRICO S.A'),(4,3,1,'yepergas','YEPERGAS S.A'),(5,2,1,'quiriquiriGas','QUIRIQUIRE GAS.SA'),(6,4,1,'enagas','ENTE NACIONAL DEL GAS'),(7,5,1,'samh','SERVICIO AUTÓNOMO DE METROLOGÍA DE HIDROCARBURO'),(8,6,1,'oroNegro','FUNDACIÓN ORO NEGRO'),(9,6,1,'alquitrana','FUNDACIÓN GUARDERÍA INFANTIL LA ALQUITRANA'),(10,6,1,'misionRibas','FUNDACIÓN MISIÓN RIBAS'),(11,6,1,'misionPiar','FUNDACIÓN MISIÓN PIAR'),(12,7,1,'inc','INSTITUTO NACIONAL DE CANALIZACIONES'),(13,7,1,'ingeomin','INSTITUTO NACIONAL DE GEOLOGÍA Y MINERÍA'),(14,2,1,'pdvsa','PETRÓLEOS DE VENEZUELA S.A'),(15,2,1,'pequiven','PETROQUÍMICA DE VENEZUELA'),(16,2,1,'cjaa','COMPLEJO PETROQUÍMICO G/D'),(17,2,1,'ducolsa','DESARROLLOS URBANOS DE LA COSTA ORIENTAL DEL LAGO'),(18,2,1,'minerven','COMPAÑÍA GENERAL DE MINERÍA DE VENEZUELA (MINERVEN)'),(19,2,1,'cvgTecmin','CVG TECNICA MINERA C.A. (CVG TECMIN)');
/*!40000 ALTER TABLE `tbl_organizaciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-09 12:58:59
